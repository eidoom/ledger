#!/usr/bin/env python3

import sqlite3, argparse, pathlib

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Tally up all transfers between two accounts"
    )

    parser.add_argument("origin", type=str, help="Origin account (from)")
    parser.add_argument("destination", type=str, help="Destination account (to)")

    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )

    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    cur = con.cursor()

    cur.row_factory = lib.pluck_factory
    accounts = cur.execute("SELECT name FROM account").fetchall()

    if args.origin not in accounts:
        raise Exception(f"origin {args.origin} not in accounts")

    if args.destination not in accounts:
        raise Exception(f"destination {args.destination} not in accounts")

    cur.row_factory = lib.dict_factory
    res = cur.execute(
        """
        SELECT
            SUM(IIF(o.name == :o, 1, -1) * t.amount) AS amount,
            c.code AS currency
        FROM transfer AS t
        INNER JOIN account AS o ON o.id = t.origin_id
        INNER JOIN account AS d ON d.id = t.destination_id
        INNER JOIN currency AS c ON c.id = t.currency_id
        WHERE
            (o.name = :o AND d.name = :d)
        OR
            (o.name = :d AND d.name = :o)
        GROUP BY c.id
        """,
        {"o": args.origin, "d": args.destination},
    ).fetchall()

    print(f"{args.origin} -> {args.destination}")

    for r in res:
        print(f"{r['amount']:8.2f} {r['currency']}")
