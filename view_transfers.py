#!/usr/bin/env python3

import sqlite3, argparse, pathlib, datetime

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="View all recorded transfers")
    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )
    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    cur = con.cursor()

    cur.row_factory = lib.dict_factory
    res = cur.execute(
        """
        SELECT
            t.id AS n,
            t.time,
            o.name AS origin,
            d.name AS destination,
            t.amount,
            c.code AS currency
        FROM transfer AS t
        INNER JOIN account AS o ON o.id = t.origin_id
        INNER JOIN account AS d ON d.id = t.destination_id
        INNER JOIN currency AS c ON c.id = t.currency_id
        """
    ).fetchall()

    w = max(max(len(r[x]) for x in ("origin", "destination")) for r in res)

    for r in res:
        time = datetime.datetime.fromtimestamp(r["time"], tz=datetime.timezone.utc)
        print(
            f"{r['n']:3} : {time} : {r['origin']:{w}} -> {r['destination']:{w}} : {r['amount']:7.2f} {r['currency']}"
        )
