#!/usr/bin/env python3

import sqlite3, argparse, pathlib

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Show balances in each currency for an account"
    )

    parser.add_argument("account", type=str, help="")

    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )

    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    cur = con.cursor()

    cur.row_factory = lib.pluck_factory
    accounts = cur.execute("SELECT name FROM account").fetchall()

    if args.account not in accounts:
        raise Exception(f"account {args.account} not in accounts")

    cur.row_factory = lib.dict_factory
    res = cur.execute(
        """
        SELECT
            SUM(IIF(d.name == :a, 1, -1) * t.amount) AS amount,
            c.code AS currency
        FROM transfer AS t
        INNER JOIN account AS o ON o.id = t.origin_id
        INNER JOIN account AS d ON d.id = t.destination_id
        INNER JOIN currency AS c ON c.id = t.currency_id
        WHERE o.name = :a OR d.name = :a
        GROUP BY c.id
        """,
        {"a": args.account},
    ).fetchall()

    print(f"balance of {args.account}")

    for r in res:
        print(f"{r['amount']:8.2f} {r['currency']}")
