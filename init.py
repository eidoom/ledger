#!/usr/bin/env python3

import sqlite3, pathlib, argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Initialise the ledger database")

    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )

    args = parser.parse_args()

    if args.ledger.is_file():
        raise Exception(f"Database file {args.ledger} already exists")

    con = sqlite3.connect(args.ledger)
    cur = con.cursor()

    schema = pathlib.Path("schema.sql").read_text()

    cur.executescript(schema)

    print(f"Created new database file {args.ledger}")
