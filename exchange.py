#!/usr/bin/env python3

import sqlite3, argparse, pathlib, urllib.request, json

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Exchange between two currencies on an account by a pair of transfers with another account"
    )

    parser.add_argument("origin_account", type=str, help="")
    parser.add_argument("destination_account", type=str, help="")

    parser.add_argument("origin_currency", type=str, help="")
    parser.add_argument("destination_currency", type=str, help="")

    parser.add_argument(
        "amount",
        type=float,
        help="Amount to exchange in original_currency (switch accounts to measure in other currency)",
    )

    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )
    parser.add_argument("-f", "--force", action="store_true", help="Skip confirmation")

    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    con.row_factory = lib.pluck_factory

    cur = con.cursor()

    accounts = cur.execute("SELECT name FROM account").fetchall()

    if args.origin_account not in accounts:
        raise Exception(f"origin {args.origin_account} not in accounts")

    if args.destination_account not in accounts:
        raise Exception(f"destination {args.destination_account} not in accounts")

    currencies = cur.execute("SELECT name FROM currency").fetchall()

    if args.origin_currency not in currencies:
        raise Exception(f"currency {args.origin_currency} not in currencies")

    if args.destination_currency not in currencies:
        raise Exception(f"currency {args.destination_currency} not in currencies")

    api_key = pathlib.Path("api_key.txt").read_text().strip()
    url = f"https://v6.exchangerate-api.com/v6/{api_key}/latest/{args.origin_currency}"
    raw = urllib.request.urlopen(url).read().decode()
    data = json.loads(raw)
    if data["result"] != "success":
        raise Exception("Currency exchange rate API call failed")
    rate = data["conversion_rates"][args.destination_currency]

    destination_amount = rate * args.amount

    print(
        f"{args.origin_account} exchanging {args.amount} {args.origin_currency} for {destination_amount:.2f} {args.destination_currency} through {args.destination_account}:"
    )
    print(
        f"    exchange rate 1 {args.origin_currency} -> {rate} {args.destination_currency}"
    )
    print(
        f"    {args.origin_account} sending {args.amount} {args.origin_currency} to {args.destination_account}"
    )
    print(
        f"    {args.destination_account} sending {destination_amount:.2f} {args.destination_currency} to {args.origin_account}"
    )

    if not args.force and input("confirm? [y/N] ") != "y":
        print("Cancelled")
        exit()

    origin_account_id = cur.execute(
        "SELECT id FROM account WHERE name = ?", (args.origin_account,)
    ).fetchone()
    destination_account_id = cur.execute(
        "SELECT id FROM account WHERE name = ?", (args.destination_account,)
    ).fetchone()

    origin_currency_id = cur.execute(
        "SELECT id FROM currency WHERE code = ?", (args.origin_currency,)
    ).fetchone()
    destination_currency_id = cur.execute(
        "SELECT id FROM currency WHERE code = ?", (args.destination_currency,)
    ).fetchone()

    cur.execute(
        """
        INSERT INTO transfer (
            origin_id,
            destination_id,
            amount,
            currency_id
        )
        VALUES (?, ?, ?, ?)
        """,
        (
            origin_account_id,
            destination_account_id,
            args.amount,
            origin_currency_id,
        ),
    )
    send_id = cur.lastrowid

    cur.execute(
        """
        INSERT INTO transfer (
            origin_id,
            destination_id,
            amount,
            currency_id
        )
        VALUES (?, ?, ?, ?)
        """,
        (
            destination_account_id,
            origin_account_id,
            destination_amount,
            destination_currency_id,
        ),
    )
    receive_id = cur.lastrowid

    cur.execute(
        """
        INSERT INTO exchange (
            send_id,
            receive_id
        )
        VALUES (?, ?)
        """,
        (send_id, receive_id),
    )
    receive_id = cur.lastrowid

    con.commit()
    print("Transaction complete")
