#!/usr/bin/env python3

import sqlite3, argparse, pathlib

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Make a transfer of money in a currency from one account to another"
    )

    parser.add_argument("origin", type=str, help="Origin account (from)")
    parser.add_argument("destination", type=str, help="Destination account (to)")
    parser.add_argument(
        "amount", type=float, help="Quantity of money in units of currency"
    )
    parser.add_argument("currency", type=str, help="The currency code (ISO 4217)")

    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )
    parser.add_argument("-f", "--force", action="store_true", help="Skip confirmation")

    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    con.row_factory = lib.pluck_factory

    cur = con.cursor()

    accounts = cur.execute("SELECT name FROM account").fetchall()

    if args.origin not in accounts:
        raise Exception(f"origin {args.origin} not in accounts")

    if args.destination not in accounts:
        raise Exception(f"destination {args.destination} not in accounts")

    currencies = cur.execute("SELECT name FROM currency").fetchall()

    if args.currency not in currencies:
        raise Exception(f"currency {args.currency} not in currencies")

    print(f"{args.origin} sending {args.amount} {args.currency} to {args.destination}")
    if not args.force and input("confirm? [y/N] ") != "y":
        print("Cancelled")
        exit()

    origin_id = cur.execute(
        "SELECT id FROM account WHERE name = ?", (args.origin,)
    ).fetchone()
    destination_id = cur.execute(
        "SELECT id FROM account WHERE name = ?", (args.destination,)
    ).fetchone()
    currency_id = cur.execute(
        "SELECT id FROM currency WHERE code = ?", (args.currency,)
    ).fetchone()

    cur.execute(
        """
        INSERT INTO transfer (
            origin_id,
            destination_id,
            amount,
            currency_id
        )
        VALUES (?, ?, ?, ?)
        """,
        (origin_id, destination_id, args.amount, currency_id),
    )

    con.commit()
    print("Transaction complete")
