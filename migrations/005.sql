BEGIN;

CREATE TABLE new_exchange (
	send_id INTEGER NOT NULL PRIMARY KEY REFERENCES exchange (id),
	receive_id INTEGER NOT NULL UNIQUE REFERENCES exchange (id),
	CHECK(send_id != receive_id)
) STRICT;

INSERT INTO new_exchange (send_id, receive_id) SELECT send_id, receive_id FROM exchange;

DROP TABLE exchange;

ALTER TABLE new_exchange RENAME TO exchange;

COMMIT;
