BEGIN;

ALTER TABLE transfer RENAME COLUMN origin TO origin_id;
ALTER TABLE transfer RENAME COLUMN destination TO destination_id;
ALTER TABLE transfer RENAME COLUMN unit TO currency_id;

ALTER TABLE exchange RENAME COLUMN from_currency TO from_currency_id;
ALTER TABLE exchange RENAME COLUMN to_currency TO to_currency_id;

COMMIT;
