BEGIN;

CREATE TABLE new_transfer (
	id INTEGER NOT NULL PRIMARY KEY,
	time INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
	origin_id INTEGER NOT NULL REFERENCES account (id),
	destination_id INTEGER NOT NULL REFERENCES account (id),
	currency_id INTEGER NOT NULL REFERENCES currency (id),
	amount REAL NOT NULL,
	CHECK(origin_id != destination_id)
) STRICT;

INSERT INTO new_transfer SELECT * FROM transfer;

DROP TABLE transfer;

ALTER TABLE new_transfer RENAME TO transfer;

COMMIT;
