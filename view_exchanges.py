#!/usr/bin/env python3

import sqlite3, argparse, pathlib, datetime

import lib


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="View all recorded exchanges")
    parser.add_argument(
        "-l", "--ledger", type=pathlib.Path, default="ledger.db", help=""
    )
    args = parser.parse_args()

    con = sqlite3.connect(args.ledger)
    cur = con.cursor()

    cur.row_factory = lib.dict_factory
    res = cur.execute(
        """
        SELECT
            s.time,
            o.name AS origin,
            d.name AS destination,
            s.amount AS q1,
            c1.code AS c1,
            r.amount AS q2,
            c2.code AS c2
        FROM exchange AS e
        INNER JOIN transfer AS s ON s.id = e.send_id
        INNER JOIN transfer AS r ON r.id = e.receive_id
        INNER JOIN account AS o ON o.id = s.origin_id
        INNER JOIN account AS d ON d.id = s.destination_id
        INNER JOIN currency AS c1 ON c1.id = s.currency_id
        INNER JOIN currency AS c2 ON c2.id = r.currency_id
        """
    ).fetchall()

    w = max(max(len(r[x]) for x in ("origin", "destination")) for r in res)

    for r in res:
        time = datetime.datetime.fromtimestamp(r["time"], tz=datetime.timezone.utc)
        rate = r["q2"] / r["q1"]
        print(
            f"{time} : {r['origin']:{w}} via {r['destination']:{w}} : {r['q1']:.2f} {r['c1']} -> {r['q2']:.2f} {r['c2']} @ {rate} {r['c2']}/{r['c1']}"
        )
